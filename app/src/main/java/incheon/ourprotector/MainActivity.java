package incheon.ourprotector;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import org.json.JSONArray;
import org.json.JSONException;

import incheon.ourprotector.Services.EmergencyService;
import incheon.ourprotector.System.AppStatus;
import incheon.ourprotector.System.Appdb;
import incheon.ourprotector.System.NotiStatus;


public class MainActivity extends FragmentActivity {

    private TabLayout tablayout;
    private ViewPager viewpager;
    private MainAdapter adapter;
    private Appdb appdb;
    private AppStatus appStatus;
    private SharedPreferences preferences;

    private String flag = null;

    private EmergencyService em;

    ServiceConnection conn = new ServiceConnection() {
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            EmergencyService.emerBinder eb = (EmergencyService.emerBinder) service;
            em = eb.getService();
        }
        public void onServiceDisconnected(ComponentName name) {
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appdb = new Appdb(this);
        tablayout = (TabLayout) findViewById(R.id.tab_layout);
        viewpager = (ViewPager) findViewById(R.id.view_pager);
        adapter =  new MainAdapter(getSupportFragmentManager(),this);
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 1 && flag == null) {
                    adapter.getmap().MapSetting(false, null);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onResume(){
        super.onResume();
        flag = NotiStatus.getflag();
        if(flag != null) {
            NotiStatus.setflag(null);
            if(flag.equals("chat")) {
                viewpager.setCurrentItem(0);
            } else if(flag.equals("emer")) {
                String data = NotiStatus.getdata();
                String name = NotiStatus.getname();
                viewpager.setCurrentItem(1);
                appdb.emerlocainsert(appdb.lastemerlocanum() + 1,name,data);
                adapter.getmap().logset(name + "의 최근 응급상황 위치");
                try {
                    adapter.getmap().MapSetting(true, new JSONArray(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(appStatus == null) {
            appStatus = AppStatus.getInstance();
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                appStatus.setIsemerbutton(preferences.getBoolean("emerb",true));
                appStatus.setIsemershake(preferences.getBoolean("emers",false));
                appStatus.setIsemerpower(preferences.getBoolean("emerp",false));
                bindService(new Intent(MainActivity.this,EmergencyService.class), conn, Context.BIND_AUTO_CREATE);
            }

            @Override
            protected Void doInBackground(Void... voids) {
                while (em == null) {}
                em.optionset();
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
                unbindService(conn);
            }
        }.execute();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
}
