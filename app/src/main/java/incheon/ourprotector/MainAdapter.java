package incheon.ourprotector;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import incheon.ourprotector.Tabs.Chating;
import incheon.ourprotector.Tabs.Emernetworks;
import incheon.ourprotector.Tabs.Maps;
import incheon.ourprotector.Tabs.SMSnetwork;
import incheon.ourprotector.Tabs.Setting;

/**
 * Created by kyun on 2016-12-03.
 */

public class MainAdapter extends FragmentPagerAdapter {

    private Maps maps;
    private Chating chating;
    private SMSnetwork SMSnetwork;
    private Emernetworks emernetworks;
    private Setting setting;

    public MainAdapter (FragmentManager fm, Context context) {
        super(fm);
        maps = new Maps(context);
        chating = new Chating(context);
        SMSnetwork = new SMSnetwork(context);
        emernetworks = new Emernetworks(context);
        setting = new Setting(context);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return chating;
            case 1:
                return maps;
            case 2:
                return SMSnetwork;
            case 3:
                return emernetworks;
            case 4:
                return setting;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "대화";
            case 1:
                return "지도";
            case 2:
                return "sms연락망";
            case 3:
                return  "주요 연락망";
            case 4:
                return "설정";
        }
        return null;
    }

    public Maps getmap() {
        return maps;
    }

    @Override
    public int getCount() {
        return 5;
    }
}
