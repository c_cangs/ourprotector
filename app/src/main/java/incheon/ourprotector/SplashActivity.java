package incheon.ourprotector;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import incheon.ourprotector.Services.EmergencyService;
import incheon.ourprotector.Services.LocationService;
import incheon.ourprotector.Services.MqttService;
import incheon.ourprotector.System.AppStatus;
import incheon.ourprotector.System.AsyncTaskCancelTimerTask;

/**
 * Created by kyun on 2016-12-02.
 */

public class SplashActivity extends Activity {
    private boolean isallgrantp = true;
    private SharedPreferences preferences;
    private AppStatus appStatus;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if(appStatus == null) {
            appStatus = AppStatus.getInstance();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { //퍼미션 요청
            requestPermissions(new String[]{android.Manifest.permission.WAKE_LOCK, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_NETWORK_STATE
                    , android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.INTERNET,
                    android.Manifest.permission.ACCESS_FINE_LOCATION,android.Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS}, 0);
        }
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        appStatus.setIsemerbutton(preferences.getBoolean("emerb",true));
        appStatus.setIsemershake(preferences.getBoolean("emers",false));
        appStatus.setIsemerpower(preferences.getBoolean("emerp",false));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        for(int i = 0; i < grantResults.length; i++){
            if(grantResults[i] == PackageManager.PERMISSION_DENIED){
                isallgrantp = false;
                //퍼미션 안된거 재요청
                break;
            }
        }
        if(isallgrantp){
            new AsyncTaskCancelTimerTask(new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    dialog = ProgressDialog.show(SplashActivity.this, "", "로딩중입니다");
                    startService(new Intent(SplashActivity.this, MqttService.class));
                    startService(new Intent(SplashActivity.this, LocationService.class));
                    startService(new Intent(SplashActivity.this, EmergencyService.class));
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    while (!AppStatus.getconn()) {}
                    return null;
                }

                @Override
                protected void onPostExecute(Void v) {
                    dialog.dismiss();
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
            }.execute(), 10000, 1000, new AsyncTaskCancelTimerTask.onCanceldo() {
                @Override
                public void onCancel() {
                    dialog.dismiss();
                    stopService(new Intent(SplashActivity.this, MqttService.class));
                    stopService(new Intent(SplashActivity.this, LocationService.class));
                    stopService(new Intent(SplashActivity.this, EmergencyService.class));
                    Toast.makeText(SplashActivity.this,"시간초과하였습니다 다시 시도해주세요",Toast.LENGTH_LONG).show();
                    finish();
                }
            }).start();
        }
    }
}
