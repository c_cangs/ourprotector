package incheon.ourprotector.System;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.widget.Toast;

/**
 * Created by kyun on 2016-11-26.
 */

public class AppStatus {
    private volatile static AppStatus instance;

    private AppStatus () {}

    public static AppStatus getInstance (){
        if(instance == null) {
            synchronized (AppStatus.class) {
                if(instance == null) {
                    instance = new AppStatus();
                }
            }
        }
        return instance;
    }

    private static String clientid;
    private static boolean chatalive = false;
    private static boolean isconn = false;
    private static boolean isemerpower = false;
    private static boolean isemershake = false;
    private static boolean isemerbutton = false;
    private static boolean isappon = false;
    private static int notiid = 0;

    public  static void setclientid (String i ) {
        clientid = i;
    }

    public static void setchatalive(boolean b){
        chatalive = b;
    }

    public static void setconn(boolean b){
        isconn = b;
    }

    public static void setIsemerpower(boolean b) {
        isemerpower = b;
    }

    public static void setIsemershake(boolean b) {
        isemershake = b;
    }

    public static void setIsemerbutton(boolean b) {
        isemerbutton = b;
    }

    public static void setIsappon(boolean b) {
        isappon = b;
    }

    public static int nextid (){
        notiid = notiid + 1;
        if(notiid == 5) {
            notiid = 0;
        }
        return notiid;
    }

    public static String getclienid () {
        return clientid;
    }

    public static boolean getchatalive() {
        return chatalive;
    }

    public static boolean getconn() {
        return isconn;
    }

    public static boolean getisemerpower() {
        return isemerpower;
    }

    public static boolean getisemershake(){
        return isemershake;
    }

    public static boolean getisemerbutton(){
        return isemerbutton;
    }

    public static boolean getisappon() {
        return isappon;
    }

    public static void sendSMS(final Context mContext, String smsNumber, String smsText){
        PendingIntent sentIntent = PendingIntent.getBroadcast(mContext, 0, new Intent("SMS_SENT_ACTION"), 0);
        PendingIntent deliveredIntent = PendingIntent.getBroadcast(mContext, 0, new Intent("SMS_DELIVERED_ACTION"), 0);

        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch(getResultCode()){
                    case Activity.RESULT_OK:
                        // 전송 성공
                        Toast.makeText(mContext, "전송 완료", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        // 전송 실패
                        Toast.makeText(mContext, "전송 실패", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        // 서비스 지역 아님
                        Toast.makeText(mContext, "서비스 지역이 아닙니다", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        // 무선 꺼짐
                        Toast.makeText(mContext, "무선(Radio)가 꺼져있습니다", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        // PDU 실패
                        Toast.makeText(mContext, "PDU Null", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter("SMS_SENT_ACTION"));

        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()){
                    case Activity.RESULT_OK:
                        // 도착 완료
                        Toast.makeText(mContext, "SMS 도착 완료", Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        // 도착 안됨
                        Toast.makeText(mContext, "SMS 도착 실패", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter("SMS_DELIVERED_ACTION"));

        SmsManager mSmsManager = SmsManager.getDefault();
        mSmsManager.sendTextMessage(smsNumber, null, smsText, sentIntent, deliveredIntent);
    }

}
