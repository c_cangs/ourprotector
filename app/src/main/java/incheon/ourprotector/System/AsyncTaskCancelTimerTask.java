package incheon.ourprotector.System;

import android.os.AsyncTask;
import android.os.CountDownTimer;

/**
 * Created by kyun on 2016-12-03.
 */

public class AsyncTaskCancelTimerTask extends CountDownTimer {
    private AsyncTask asyncTask;
    private boolean interrupt = true;
    private onCanceldo onCanceldo;

    public AsyncTaskCancelTimerTask(AsyncTask asyncTask, long startTime, long interval) {
        super(startTime, interval);
        this.asyncTask = asyncTask;
    }

    public AsyncTaskCancelTimerTask(AsyncTask asyncTask, long startTime, long interval, onCanceldo onCanceldo) {
        super(startTime, interval);
        this.asyncTask = asyncTask;
        this.onCanceldo = onCanceldo;
    }


    @Override
    public void onTick(long millisUntilFinished) {

        if(asyncTask == null) {
            this.cancel();
            return;
        }

        if(asyncTask.isCancelled())
            this.cancel();

        if(asyncTask.getStatus() == AsyncTask.Status.FINISHED)
            this.cancel();
    }

    @Override
    public void onFinish() {

        if(asyncTask == null || asyncTask.isCancelled() )
            return;

        try {
            if(asyncTask.getStatus() == AsyncTask.Status.FINISHED)
                return;

            if(asyncTask.getStatus() == AsyncTask.Status.PENDING ||
                    asyncTask.getStatus() == AsyncTask.Status.RUNNING ) {

                asyncTask.cancel(interrupt);
                onCanceldo.onCancel();

            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    public interface onCanceldo {
        void onCancel();
    }
}
