package incheon.ourprotector.System;

/**
 * Created by kyun on 2016-12-07.
 */

public class NotiStatus {
    private volatile static NotiStatus instance;

    private NotiStatus () {}

    public static NotiStatus getInstance (){
        if(instance == null) {
            synchronized (NotiStatus.class) {
                if(instance == null) {
                    instance = new NotiStatus();
                }
            }
        }
        return instance;
    }
    private static String name, data, flag = null;

    public static void setname(String s) {
        name = s;
    }

    public static void setdata(String s) {
        data = s;
    }

    public static void setflag(String s) {
        flag = s;
    }

    public static String getname() {
        return name;
    }

    public static String getdata() {
        return data;
    }

    public static String getflag() {
        return flag;
    }
}
