package incheon.ourprotector.System;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by kyun on 2016-11-25.
 */

public class Appdb extends SQLiteOpenHelper {


    public Appdb(Context context) {
        super(context, "app.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table mqttlist (topic text primary key, name text);");
        db.execSQL("create table chat (num integer primary key, name text, txt text);");
        db.execSQL("create table location (num integer primary key, latitude real, longtitude real,time text);");
        db.execSQL("create table emerloca (num integer primary key, name text, jsondata text);");
        db.execSQL("create table sms (ph text primary key not null,name text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void mqttinsert(String topic, String name) {
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("insert into mqttlist values ('"+ topic +"','" + name + "');");
        db.close();
    }

    public String[] chatinsert (int num, String name, String txt) {
        SQLiteDatabase db = getWritableDatabase();
        String[] nowchat = new String[2];
        db.execSQL("insert into chat values ('"+ num +"','" + name + "','"+txt+"');");
        nowchat[0] = name;
        nowchat[1] = txt.replaceAll("''","'");
        db.close();
        return nowchat;
    }

    public void locationinsert (int num, double latitude, double longtitude, String time){
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("insert into location values ('"+ num +"','" + latitude + "','" + longtitude + "','" + time + "');");
        db.close();

    }

    public void emerlocainsert (int num, String name, String data){
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("insert into emerloca values ('"+ num +"','" + name + "','" + data + "');");
        db.close();

    }

    public void smsinsert(String ph, String name) {
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("insert into sms values ('"+ ph +"','" + name + "');");
        db.close();
    }

    public void mqttdelete(String s) {
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("delete from mqttlist where topic = '" + s + "';");
        db.close();
    }

    public void emerdelete(int i) {
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("delete from emerloca where num = '" + i + "';");
        db.close();
    }

    public void smsdelete(String ph) {
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("delete from sms where ph = '" + ph + "';");
        db.close();
    }

    public void smsdeleteall() {
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("delete from sms;");
        db.close();
    }

    public Cursor getmqtt(){
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from mqttlist", null);

        return cursor;
    }

    public Cursor getchats () {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from chat", null);

        return cursor;
    }

    public Cursor getlocaions() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from location", null);

        return cursor;
    }

    public Cursor getemerloca(int i) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from emerloca where num = '" + i + "'", null);

        return cursor;
    }

    public Cursor getallemerloca(){
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from emerloca", null);

        return cursor;
    }

    public Cursor getsms () {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from sms", null);

        return cursor;
    }

    public int lastchatnum() {
        SQLiteDatabase db = getReadableDatabase();
        int result = 0;

        Cursor cursor = db.rawQuery("select num from chat where num in(select MAX(num) from chat);", null);
        while(cursor.moveToNext()) {
            result = cursor.getInt(0);
        }
        cursor.close();
        db.close();

        return result;
    }

    public int lastlocationnum(){
        SQLiteDatabase db = getReadableDatabase();
        int result = 0;

        Cursor cursor = db.rawQuery("select num from location where num in(select MAX(num) from location);", null);
        while(cursor.moveToNext()) {
            result = cursor.getInt(0);
        }
        cursor.close();
        db.close();

        return result;
    }

    public int lastemerlocanum() {
        SQLiteDatabase db = getReadableDatabase();
        int result = 0;

        Cursor cursor = db.rawQuery("select num from emerloca where num in(select MAX(num) from emerloca);", null);
        while(cursor.moveToNext()) {
            result = cursor.getInt(0);
        }
        cursor.close();
        db.close();

        return result;
    }
}
