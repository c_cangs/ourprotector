package incheon.ourprotector.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import incheon.ourprotector.MainActivity;
import incheon.ourprotector.R;
import incheon.ourprotector.System.AppStatus;
import incheon.ourprotector.System.Appdb;
import incheon.ourprotector.System.NotiStatus;

import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;

/**
 * Created by kyun on 2016-11-25.
 */

public class MqttService extends Service{
    private MqttAndroidClient mqttAndroidClient;
    private MqttConnectOptions mqttConnectOptions;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private String[] topics, names;
    private Appdb Appdb;
    private Context mcontext;
    private AppStatus appStatus;
    private NotiStatus notiStatus;

    private final String serverUri = "tcp://kyunhm.iptime.org:1883";
    private String clientId;

    public interface ICallback {
        void sendchat(String name,String txt);
        void subresult(boolean result);
    }

    private ICallback mCallback;

    IBinder mBinder = new MqttBinder();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class MqttBinder extends Binder {
        public MqttService getService() { // 서비스 객체를 리턴
            return MqttService.this;
        }
    }

    public void registerCallback(ICallback cb) {
        mCallback = cb;
    }


    @Override
    public void onCreate(){
        this.mcontext = this;
        if(appStatus == null) {
            appStatus = AppStatus.getInstance();
        }
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        clientId = preferences.getString("clientid","");
        appStatus.setclientid(clientId);
        Appdb = new Appdb(this);
        Cursor list = Appdb.getmqtt();
        topics = new String[list.getCount()];
        names = new String[list.getCount()];
        while(list.moveToNext()){
            topics[list.getPosition()] = list.getString(0);
            names[list.getPosition()] = list.getString(1);
            if(list.getPosition() == list.getCount() - 1){
                list.close();
            }
        }

        if(clientId.equals("")){
            if (Build.VERSION.SDK_INT != Build.VERSION_CODES.LOLLIPOP_MR1) {
                clientId = Settings.Secure.getString(this.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
            } else {
                TelephonyManager telephony = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
                clientId = telephony.getDeviceId();
            }
            appStatus.setclientid(clientId);
            editor.putString("clientid",clientId);
            editor.commit();
        }

        mqttAndroidClient = new MqttAndroidClient(this, serverUri, clientId);
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {

                if (reconnect) {
                    //addToHistory("Reconnected to : " + serverURI);
                    // Because Clean Session is true, we need to re-subscribe
                    if(!topics[0].equals(null)){
                        for(int i = 0;i < topics.length ; i++) {
                            subscribeToTopic(topics[i]);
                            subscribeToTopic(topics[i] + "chat");
                        }
                    }
                } else {
                    //addToHistory("Connected to: " + serverURI);
                }
            }

            @Override
            public void connectionLost(Throwable cause) {
                //addToHistory("The Connection was lost.");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                //addToHistory("Incoming message: " + new String(message.getPayload()));
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

        mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);

        try {
            //addToHistory("Connecting to " + serverUri);
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    appStatus.setconn(true);
                    Toast.makeText(getApplicationContext(),"서버연결성공",Toast.LENGTH_LONG).show();
                    if(!(topics.length == 0)){
                        for(int i = 0;i < topics.length ; i++) {
                            subscribeToTopic(topics[i]);
                            subscribeToTopic(topics[i] + "chat");
                        }
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // addToHistory("Failed to connect to: " + serverUri);
                    appStatus.setconn(false);
                }
            });
        } catch (MqttException ex){
            ex.printStackTrace();
        }
    }



    @Override
    public void onDestroy() {
        try {
            mqttAndroidClient.disconnect();
            appStatus.setconn(false);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void subscribeToTopic(final String topic){
        try {
            mqttAndroidClient.subscribe(topic, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    //addToHistory("Subscribed!");
                    if(mCallback != null) {
                        Cursor list = Appdb.getmqtt();
                        topics = new String[list.getCount()];
                        names = new String[list.getCount()];
                        while(list.moveToNext()){
                            topics[list.getPosition()] = list.getString(0);
                            names[list.getPosition()] = list.getString(1);
                        }
                        mCallback.subresult(true);
                    }
                    try {
                        mqttAndroidClient.subscribe(topic, 0, new IMqttMessageListener() {
                            @Override
                            public void messageArrived(String topic, MqttMessage message) throws Exception {
                                // message Arrived!
                                String mess = new String(message.getPayload());
                                if(topic.toString().matches(".*chat.*")) {
                                    for (int i = 0; i < topics.length; i++) {
                                        if (topic.toString().matches(".*" + topics[i] + ".*")) {
                                            Appdb = new Appdb(mcontext);
                                            Appdb.chatinsert(Appdb.lastchatnum() + 1, names[i], mess);
                                            if (appStatus.getchatalive()) {
                                                mCallback.sendchat(names[i], mess);
                                            } else {
                                                Noti(names[i], mess , "chat",null);
                                            }
                                            break;
                                        }
                                    }
                                }
                                else{
                                    //위급상황
                                    for (int i = 0; i < topics.length; i++) {
                                        if (topic.toString().matches(".*" + topics[i] + ".*")) {
                                            Noti(names[i], "위급 상황에 처했습니다 도와주세요", "emer",mess);
                                            break;
                                        }
                                    }
                                }
                            }
                        });
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    //addToHistory("Failed to subscribe");
                    if(mCallback != null) {
                        mCallback.subresult(false);
                    }
                }
            });


        } catch (MqttException ex){
            System.err.println("Exception whilst subscribing");
            ex.printStackTrace();
        }
    }

    public void publishchatMessage(String s){
        try {
            MqttMessage message = new MqttMessage();
            message.setPayload(s.getBytes()); //publishMessage가 보낼꺼
            mqttAndroidClient.publish(clientId + "chat", message);
            //addToHistory("Message Published");
            if(!mqttAndroidClient.isConnected()){
                //addToHistory(mqttAndroidClient.getBufferedMessageCount() + " messages in buffer.");
            }
        } catch (MqttException e) {
            System.err.println("Error Publishing: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void publishemerMessage(String s){
        try {
            MqttMessage message = new MqttMessage();
            message.setPayload(s.getBytes()); //publishMessage가 보낼꺼
            mqttAndroidClient.publish(clientId, message);
            //addToHistory("Message Published");
            if(!mqttAndroidClient.isConnected()){
                //addToHistory(mqttAndroidClient.getBufferedMessageCount() + " messages in buffer.");
            }
        } catch (MqttException e) {
            System.err.println("Error Publishing: " + e.getMessage());
            e.printStackTrace();
        }
    }


    public void Noti(String title, String msg, String des, String mess) {
        if(notiStatus == null) {
            notiStatus = NotiStatus.getInstance();
        }
        Intent i = null;
        if (des.equals("chat")) {
            i = new Intent(this,MainActivity.class).addFlags(FLAG_ACTIVITY_SINGLE_TOP);
            notiStatus.setflag("chat");
        } else if (des.equals("emer")) {
            //긴급상황으로 이동
            i = new Intent(this,MainActivity.class).addFlags(FLAG_ACTIVITY_SINGLE_TOP);
            notiStatus.setflag("emer");
            notiStatus.setdata(mess);
            notiStatus.setname(title);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(msg)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_LIGHTS);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        notificationManager.notify(appStatus.nextid(), notificationBuilder.build());

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone ring = RingtoneManager.getRingtone(getApplicationContext(), defaultSoundUri);
        ring.play(); //소리

        ((Vibrator) getApplicationContext().getSystemService(
                Context.VIBRATOR_SERVICE)).vibrate(800); //진동


    }


}
