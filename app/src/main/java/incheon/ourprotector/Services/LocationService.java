package incheon.ourprotector.Services;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;

import java.util.Calendar;

import incheon.ourprotector.System.Appdb;

/**
 * Created by kyun on 2016-12-01.
 */

public class LocationService extends Service implements ConnectionCallbacks, OnConnectionFailedListener {
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private Appdb appdb;
    private Handler taskhandler;

    private boolean isConnected = false;


    @Override
    public void onCreate() {
        localsetting();
        appdb = new Appdb(this);

        taskhandler = new Handler()
        {
            public void handleMessage(Message msg)
            {
                super.handleMessage(msg);
                getlocation();
                this.sendEmptyMessageDelayed(0, 3000000);
            }
        };
    }

    private void localsetting() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                mGoogleApiClient = new GoogleApiClient.Builder(LocationService.this)
                        .addConnectionCallbacks(LocationService.this)
                        .addOnConnectionFailedListener(LocationService.this)
                        .addApi(LocationServices.API)
                        .build();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
                mGoogleApiClient.connect();
            }
        }.execute();
    }

    private void getlocation () {
        if (ActivityCompat.checkSelfPermission(LocationService.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(LocationService.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && isConnected) {
            mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if(mLocation != null) {
                Calendar now = Calendar.getInstance();
                String nowtime = Integer.toString(now.get(Calendar.YEAR)) + "-" + Integer.toString(now.get(Calendar.MONTH) + 1) + "-" + Integer.toString(now.get(Calendar.DAY_OF_MONTH)) + " "
                        + Integer.toString(now.get(Calendar.HOUR_OF_DAY)) + ":" + Integer.toString(now.get(Calendar.MINUTE));
                appdb.locationinsert(appdb.lastlocationnum() + 1, mLocation.getLatitude(), mLocation.getLongitude(), nowtime);
            }
        }
    }


    @Override
    public void onDestroy() {
        isConnected = false;
        taskhandler.removeMessages(0);
        mGoogleApiClient.disconnect();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        isConnected = true;
        taskhandler.sendEmptyMessage(0);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        isConnected = false;
    }


}
