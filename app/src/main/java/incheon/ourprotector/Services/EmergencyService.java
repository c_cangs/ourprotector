package incheon.ourprotector.Services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import incheon.ourprotector.System.AppStatus;
import incheon.ourprotector.System.Appdb;
import incheon.ourprotector.System.AsyncTaskCancelTimerTask;

/**
 * Created by kyun on 2016-12-05.
 */

public class EmergencyService extends Service implements SensorEventListener {

    private boolean isemerlisten = false;
    private boolean isemersend = false;
    private boolean isserviceshakeon = false;
    private boolean isservicebuttonon = false;
    private boolean isservicepoweron = false;

    private int count = 0;
    private long lastTime;
    private float speed;
    private float lastX;
    private float lastY;
    private float lastZ;
    private float x, y, z;

    private static final int SHAKE_THRESHOLD = 800;
    private static final int DATA_X = SensorManager.DATA_X;
    private static final int DATA_Y = SensorManager.DATA_Y;
    private static final int DATA_Z = SensorManager.DATA_Z;
    private SensorManager sensorManager;
    private Sensor accelerormeterSensor;

    private MqttService ms;
    private Appdb appdb;
    private AppStatus appStatus;

    ServiceConnection conn = new ServiceConnection() {
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            MqttService.MqttBinder mb= (MqttService.MqttBinder) service;
            ms = mb.getService();
        }
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    IBinder mBinder = new emerBinder();

    public class emerBinder extends Binder {
        public EmergencyService getService() { // 서비스 객체를 리턴
            return EmergencyService.this;
        }
    }


    @Override
    public void onCreate (){
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerormeterSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        appdb = new Appdb(this);
        if(appStatus == null) {
            appStatus = AppStatus.getInstance();
        }
        optionset();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void optionset () {
        if(appStatus.getisemerpower()) {
            //전원 off
            if(!isservicepoweron) {
                isservicepoweron = true;
            }
        } else if (!appStatus.getisemerpower() && isservicepoweron) {
            isservicepoweron = false;
        }
        if(appStatus.getisemerbutton()){
            //전원버튼
            if(!isservicebuttonon) {
                IntentFilter offfilter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
                registerReceiver(emerbutton, offfilter);
                isservicebuttonon = true;
            }
        } else if (!appStatus.getisemerbutton() && isservicebuttonon){
            unregisterReceiver(emerbutton);
            isservicebuttonon = false;
        }
        if(appStatus.getisemershake()){
            //흔들기
            if(!isserviceshakeon) {
                if (accelerormeterSensor != null) {
                    sensorManager.registerListener(this, accelerormeterSensor, SensorManager.SENSOR_DELAY_GAME);
                    isserviceshakeon = true;
                }
            }
        } else if(!appStatus.getisemershake() && isserviceshakeon) {
            sensorManager.unregisterListener(this);
            isserviceshakeon = false;
        }
    }

    private void emermqtt () {
        new AsyncTask<Void, Void, Void>() { //위급전송
            @Override
            protected void onPreExecute() {
                bindService(new Intent(EmergencyService.this, MqttService.class), conn, Context.BIND_AUTO_CREATE);
                Cursor allsms = appdb.getsms();
                while (allsms.moveToNext()) {
                    String ph = allsms.getString(0);
                    String name = allsms.getString(1);
                    appStatus.sendSMS(EmergencyService.this, ph, "위급상황입니다 도와주세요");
                }
            }
            @Override
            protected Void doInBackground(Void... voids) {
                while(ms ==null) {}
                Cursor myloca = appdb.getlocaions();
                String locas = null;
                while (myloca.moveToNext()) {
                    int i = myloca.getInt(0);
                    double lati = myloca.getDouble(1);
                    double loti = myloca.getDouble(2);
                    String time = myloca.getString(3);
                    if (myloca.getPosition() == myloca.getCount() - 1 && myloca.getPosition() == 0) {
                        locas = "[{\"num\":" + "\"" + i + "\"," + "\"lati\":" + "\"" + lati + "\"," + "\"loti\":" + "\"" + loti + "\"," + "\"time\":" + "\"" + time + "\"}]";
                    } else if (myloca.getPosition() == 0) {
                        locas = "[{\"num\":" + "\"" + i + "\"," + "\"lati\":" + "\"" + lati + "\"," + "\"loti\":" + "\"" + loti + "\"," + "\"time\":" + "\"" + time + "\"},";
                    } else if (myloca.getPosition() == myloca.getCount() - 1) {
                        locas = locas + "{\"num\":" + "\"" + i + "\"," + "\"lati\":" + "\"" + lati + "\"," + "\"loti\":" + "\"" + loti + "\"," + "\"time\":" + "\"" + time + "\"}]";
                        ms.publishemerMessage(locas);
                    } else {
                        locas = locas + "{\"num\":" + "\"" + i + "\"," + "\"lati\":" + "\"" + lati + "\"," + "\"loti\":" + "\"" + loti + "\"," + "\"time\":" + "\"" + time + "\"},";
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
                unbindService(conn);
            }
        }.execute();
    }



    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            long currentTime = System.currentTimeMillis();
            long gabOfTime = (currentTime - lastTime);
            if (gabOfTime > 100) {
                lastTime = currentTime;
                x = sensorEvent.values[SensorManager.DATA_X];
                y = sensorEvent.values[SensorManager.DATA_Y];
                z = sensorEvent.values[SensorManager.DATA_Z];

                speed = Math.abs(x + y + z - lastX - lastY - lastZ) / gabOfTime * 10000;

                if (speed > SHAKE_THRESHOLD) {
                    if(!isemerlisten) {
                        isemerlisten = true;
                        new AsyncTaskCancelTimerTask(new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {
                                while(isemerlisten){
                                    if(!isemersend) {
                                        emermqtt();
                                        isemersend = true;
                                    }
                                }
                                return null;
                            }
                        }.execute(), 300000, 1000, new AsyncTaskCancelTimerTask.onCanceldo() {
                            @Override
                            public void onCancel() {
                                isemerlisten = false;
                                isemersend = false;
                            }
                        }).start();
                    }

                }

                lastX = sensorEvent.values[DATA_X];
                lastY = sensorEvent.values[DATA_Y];
                lastZ = sensorEvent.values[DATA_Z];
            }

        }
    }

    private BroadcastReceiver emerbutton = new BroadcastReceiver(){
        public static final String Screenoff = "android.intent.action.SCREEN_OFF";

        @Override
        public void onReceive(Context context, Intent intent){
            if(!intent.getAction().equals(Screenoff))return;
            count ++;
            if(!isemerlisten){
                isemerlisten = true;
                new AsyncTaskCancelTimerTask(new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        while(isemerlisten) {
                            if (count >= 3) {
                                if(!isemersend) {
                                    emermqtt();
                                    isemersend = true;
                                }
                            }
                        }
                        return null;
                    }
                }.execute(), 8000, 1000, new AsyncTaskCancelTimerTask.onCanceldo() {
                    @Override
                    public void onCancel() {
                        isemerlisten = false;
                        isemersend = false;
                        count = 0;
                    }
                }).start();
            }
        }
    };

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
