package incheon.ourprotector.Tabs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import incheon.ourprotector.R;
import incheon.ourprotector.System.Appdb;


/**
 * Created by kyun on 2016-12-03.
 */

@SuppressLint("ValidFragment")
public class SMSnetwork extends Fragment {

    private Appdb appdb;
    private LinearLayout smslist;
    private Button smsdeleteall, addsms;
    private Context mContext;
    private LayoutInflater inflater;


    public SMSnetwork(Context context) {
        this.mContext = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        appdb = new Appdb(context);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_sms,null);
        smslist = (LinearLayout) v.findViewById(R.id.sms_list);
        smsdeleteall = (Button) v.findViewById(R.id.sms_deleteall);
        addsms = (Button) v.findViewById(R.id.add_sms);
        smsdeleteall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appdb.smsdeleteall();
                smslist.removeAllViews();
            }
        });

        addsms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View dialogView = inflater.inflate(R.layout.dialog_addnetwork, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setView(dialogView);
                final AlertDialog userdialog = builder.create();
                userdialog.setCanceledOnTouchOutside(true);
                userdialog.setTitle("sms 연락망 등록");
                userdialog.show();
                final EditText addname = (EditText) dialogView.findViewById(R.id.add_name);
                final EditText addph = (EditText) dialogView.findViewById(R.id.add_ph);
                Button addbutton = (Button) dialogView.findViewById(R.id.add_button);
                addname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                        switch (i){
                            case EditorInfo.IME_ACTION_NEXT :
                                addph.requestFocus();
                        }
                        return false;
                    }
                });
                addph.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                        switch (i) {
                            case EditorInfo.IME_ACTION_DONE :
                                addlist(addph.getText().toString(),addname.getText().toString());
                                userdialog.dismiss();
                        }
                        return false;
                    }
                });
                addbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addlist(addph.getText().toString(),addname.getText().toString());
                        userdialog.dismiss();
                    }
                });
            }
        });
        Cursor allsms = appdb.getsms();
        while (allsms.moveToNext()){
            final String ph = allsms.getString(0);
            final String name = allsms.getString(1);
            final View addsmsview = inflater.inflate(R.layout.layout_networkitem,null);
            TextView smsname = (TextView) addsmsview.findViewById(R.id.net_name);
            TextView phonenum = (TextView) addsmsview.findViewById(R.id.net_num);
            Button smsdelete = (Button) addsmsview.findViewById(R.id.net_delete);
            smsname.setText(name);
            phonenum.setText(ph);
            smsdelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appdb.smsdelete(ph);
                    smslist.removeView(addsmsview);
                }
            });
            smslist.addView(addsmsview);
        }
        return v;
    }

    public void addlist(final String ph, String name) {
        appdb.smsinsert(ph,name);
        final View v = inflater.inflate(R.layout.layout_networkitem,null);
        TextView smsname = (TextView) v.findViewById(R.id.net_name);
        TextView phonenum = (TextView) v.findViewById(R.id.net_num);
        Button smsdelete = (Button) v.findViewById(R.id.net_delete);
        smsname.setText(name);
        phonenum.setText(ph);
        smsdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appdb.smsdelete(ph);
                smslist.removeView(v);
            }
        });
        smslist.addView(v);

    }

}
