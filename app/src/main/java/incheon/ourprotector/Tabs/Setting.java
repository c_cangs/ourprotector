package incheon.ourprotector.Tabs;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.print.PrintHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import incheon.ourprotector.R;
import incheon.ourprotector.Services.EmergencyService;
import incheon.ourprotector.System.AppStatus;

/**
 * Created by kyun on 2016-12-05.
 */

@SuppressLint("ValidFragment")
public class Setting extends Fragment{

    private Context mContext;
    private CheckBox epower, eshake, ebutton;
    private Button epublish;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ProgressDialog dialog;
    private AppStatus appStatus;

    private EmergencyService em;

    ServiceConnection conn = new ServiceConnection() {
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            EmergencyService.emerBinder eb = (EmergencyService.emerBinder) service;
            em = eb.getService();
        }
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    public Setting (Context context) {
        this.mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_options,null);
        if(appStatus == null) {
            appStatus = AppStatus.getInstance();
        }
        epower = (CheckBox) v.findViewById(R.id.emer_power);
        eshake = (CheckBox) v.findViewById(R.id.emer_shake);
        ebutton = (CheckBox) v.findViewById(R.id.emer_button);
        epublish = (Button) v.findViewById(R.id.emer_publish);
        epower.setChecked(appStatus.getisemerpower());
        eshake.setChecked(appStatus.getisemershake());
        ebutton.setChecked(appStatus.getisemerbutton());
        epublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean p = epower.isChecked();
                boolean s = eshake.isChecked();
                boolean b = ebutton.isChecked();
                if(!p && !s && !b) {
                    Toast.makeText(mContext,"최소 1개이상 선택하셔야합니다",Toast.LENGTH_SHORT).show();
                } else {
                    preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
                    editor = preferences.edit();
                    editor.putBoolean("emerp",p);
                    editor.putBoolean("emers",s);
                    editor.putBoolean("emerb",b);
                    editor.commit();
                    appStatus.setIsemerpower(p);
                    appStatus.setIsemershake(s);
                    appStatus.setIsemerbutton(b);
                    new AsyncTask<Void, Void, Void>() { //위급전송
                        @Override
                        protected void onPreExecute() {
                            dialog = ProgressDialog.show(mContext, "", "로딩중입니다");
                            mContext.bindService(new Intent(mContext,EmergencyService.class),conn, Context.BIND_AUTO_CREATE);
                        }

                        @Override
                        protected Void doInBackground(Void... voids) {
                            while(em == null) {}
                            em.optionset();
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void v) {
                            dialog.dismiss();
                            Toast.makeText(mContext,"설정 완료",Toast.LENGTH_SHORT).show();
                            mContext.unbindService(conn);
                        }
                    }.execute();
                }
            }
        });
        return v;
    }
}
