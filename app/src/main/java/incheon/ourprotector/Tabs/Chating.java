package incheon.ourprotector.Tabs;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import incheon.ourprotector.MainActivity;
import incheon.ourprotector.R;
import incheon.ourprotector.Services.MqttService;
import incheon.ourprotector.Services.MqttService.*;
import incheon.ourprotector.System.AppStatus;
import incheon.ourprotector.System.Appdb;

/**
 * Created by kyun on 2016-11-25.
 */

@SuppressLint("ValidFragment")
public class Chating extends Fragment {

    private TextView chattxt;
    private View chatview;
    private EditText editchat;
    private Button dochat;
    private LayoutInflater inflater;
    private LinearLayout chatlayout;
    private ScrollView chatscroll;
    private Appdb Appdb;
    private MqttService ms;
    private Context mContext;
    private AppStatus appStatus;

    private ICallback callback = new ICallback() {
        @Override
        public void sendchat(String name, String txt) {
            addchat(name,txt);
        }

        @Override
        public void subresult(boolean result) {

        }
    };

    ServiceConnection conn = new ServiceConnection() {
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            MqttBinder mb= (MqttBinder) service;
            ms = mb.getService();
            ms.registerCallback(callback);
        }
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    public Chating (Context context){
        this.mContext = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_chating,null);
        if(appStatus == null) {
            appStatus = AppStatus.getInstance();
        }
        appStatus.setchatalive(true);
        chatlayout = (LinearLayout) v.findViewById(R.id.chat_layout);
        chatscroll = (ScrollView) v.findViewById(R.id.chat_scroll);
        mContext.bindService(new Intent(mContext, MqttService.class), conn, Context.BIND_AUTO_CREATE);
        Appdb = new Appdb(mContext);

        editchat = (EditText) v.findViewById(R.id.edit_chat);
        editchat.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        chatscroll.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                }.sendEmptyMessageDelayed(0, 300);
                return false;
            }
        });
        chatscroll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager im = (InputMethodManager) mContext.getSystemService(mContext.INPUT_METHOD_SERVICE);
                im.hideSoftInputFromWindow(((MainActivity)mContext).getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });
        dochat = (Button) v.findViewById(R.id.dochat);
        dochat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mytxt = editchat.getText().toString().replaceAll("'", "''");
                if(mytxt.equals("")){
                    Toast.makeText(mContext,"내용을 입력하세요",Toast.LENGTH_SHORT).show();
                } else {
                    String[] chatcontent = Appdb.chatinsert(Appdb.lastchatnum() + 1, "me", mytxt);
                    addchat(chatcontent[0], chatcontent[1]);
                    editchat.setText(null);
                    ms.publishchatMessage(mytxt);
                }
            }
        });

        final Cursor allchat = Appdb.getchats();
        while (allchat.moveToNext()) {
            final int num = allchat.getInt(0);
            final String name = allchat.getString(1);
            final String txt = allchat.getString(2);
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    if (name.equals("me")) {
                        chatview = inflater.inflate(R.layout.layout_mychat, null);
                        chattxt = (TextView) chatview.findViewById(R.id.mychat);
                        chattxt.setText(txt);
                        chatlayout.addView(chatview);
                    } else {
                        chatview = inflater.inflate(R.layout.layout_otherchat, null);
                        chattxt = (TextView) chatview.findViewById(R.id.otherchat);
                        chattxt.setText(name + " : " + txt);
                        chatlayout.addView(chatview);
                    }
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    return null;
                }

                @Override
                protected void onPostExecute(Void v) {
                    chatscroll.fullScroll(ScrollView.FOCUS_DOWN);
                    allchat.close();
                }
            }.execute();
        }

        return v;
    }


    @Override
    public void onPause() {
        super.onPause();
        mContext.unbindService(conn);
        appStatus.setchatalive(false);
    }

    @Override
    public void onResume(){
        super.onResume();
        if(appStatus == null) {
            appStatus = AppStatus.getInstance();
        }
        mContext.bindService(new Intent(mContext, MqttService.class), conn, Context.BIND_AUTO_CREATE);
        appStatus.setchatalive(true);
        final Cursor allchat = Appdb.getchats();
        chatlayout.removeAllViews();
        while (allchat.moveToNext()) {
            final int num = allchat.getInt(0);
            final String name = allchat.getString(1);
            final String txt = allchat.getString(2);
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    if (name.equals("me")) {
                        chatview = inflater.inflate(R.layout.layout_mychat, null);
                        chattxt = (TextView) chatview.findViewById(R.id.mychat);
                        chattxt.setText(txt);
                        chattxt.setGravity(Gravity.RIGHT);
                        chatlayout.addView(chatview);
                    } else {
                        chatview = inflater.inflate(R.layout.layout_otherchat, null);
                        chattxt = (TextView) chatview.findViewById(R.id.otherchat);
                        chattxt.setText(name + " : " + txt);
                        chattxt.setGravity(Gravity.LEFT);
                        chatlayout.addView(chatview);
                    }
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    return null;
                }

                @Override
                protected void onPostExecute(Void v) {
                    chatscroll.fullScroll(ScrollView.FOCUS_DOWN);
                    allchat.close();
                }
            }.execute();
        }
    }


    private void addchat(final String name, final String txt) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                if (name.equals("me")) {
                    chatview = inflater.inflate(R.layout.layout_mychat, null);
                    chattxt = (TextView) chatview.findViewById(R.id.mychat);
                    chattxt.setText(txt);
                    chattxt.setGravity(Gravity.RIGHT);
                } else {
                    chatview = inflater.inflate(R.layout.layout_otherchat, null);
                    chattxt = (TextView) chatview.findViewById(R.id.otherchat);
                    chattxt.setText(name + " : " + txt);
                    chattxt.setGravity(Gravity.LEFT);
                }
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    ((MainActivity)mContext).runOnUiThread(new Runnable() {
                        public void run() {
                            chatlayout.addView(chatview);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void v) {
                try {
                    new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            chatscroll.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    }.sendEmptyMessageDelayed(0, 50);
                }catch (Exception e){

                }
            }
        }.execute();
    }
}
