package incheon.ourprotector.Tabs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import incheon.ourprotector.R;
import incheon.ourprotector.Services.MqttService;
import incheon.ourprotector.System.AppStatus;
import incheon.ourprotector.System.Appdb;
import incheon.ourprotector.System.AsyncTaskCancelTimerTask;

/**
 * Created by kyun on 2016-12-06.
 */

@SuppressLint("ValidFragment")
public class Emernetworks extends Fragment{

    private Appdb appdb;
    private Context mContext;
    private LinearLayout emerlist;
    private Button emer_send, emer_recev;
    private ProgressDialog dialog;
    private boolean sucsub;
    private AppStatus appStatus;

    private MqttService ms;

    private MqttService.ICallback callback = new MqttService.ICallback() {
        @Override
        public void sendchat(String name, String txt) {

        }

        @Override
        public void subresult(boolean result) {
            sucsub = result;
        }
    };

    ServiceConnection conn = new ServiceConnection() {
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            MqttService.MqttBinder mb= (MqttService.MqttBinder) service;
            ms = mb.getService();
            ms.registerCallback(callback);
        }
        public void onServiceDisconnected(ComponentName name) {
        }
    };


    public Emernetworks(Context context) {
        this.mContext = context;
        appdb = new Appdb(context);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        if(appStatus == null) {
            appStatus = AppStatus.getInstance();
        }
        View v = inflater.inflate(R.layout.tab_emernetwork,null);
        emerlist = (LinearLayout) v.findViewById(R.id.emer_list);
        emer_send = (Button) v.findViewById(R.id.add_emersend);
        emer_recev = (Button) v.findViewById(R.id.add_emerrecev);

        emer_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View dialogView = inflater.inflate(R.layout.dialog_addnetwork, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setView(dialogView);
                final AlertDialog userdialog = builder.create();
                userdialog.setCanceledOnTouchOutside(true);
                userdialog.setTitle("연락망 추가 요청");
                userdialog.show();
                final EditText addname = (EditText) dialogView.findViewById(R.id.add_name);
                final EditText addph = (EditText) dialogView.findViewById(R.id.add_ph);
                Button addbutton = (Button) dialogView.findViewById(R.id.add_button);
                addname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                        switch (i){
                            case EditorInfo.IME_ACTION_NEXT :
                                addph.requestFocus();
                        }
                        return false;
                    }
                });
                addph.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                        switch (i) {
                            case EditorInfo.IME_ACTION_DONE :
                                appStatus.sendSMS(mContext,addph.getText().toString(), appStatus.getclienid());
                        }
                        return false;
                    }
                });
                addbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        appStatus.sendSMS(mContext,addph.getText().toString(), appStatus.getclienid());
                    }
                });

            }
        });


        emer_recev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View dialogView = inflater.inflate(R.layout.dialog_addnetwork, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setView(dialogView);
                final AlertDialog userdialog = builder.create();
                userdialog.setCanceledOnTouchOutside(true);
                userdialog.setTitle("수신 연락망 등록");
                userdialog.show();
                final EditText addname = (EditText) dialogView.findViewById(R.id.add_name);
                final EditText addid = (EditText) dialogView.findViewById(R.id.add_ph);
                Button addbutton = (Button) dialogView.findViewById(R.id.add_button);
                addid.setHint("sms에서 수신한 id 입력");
                addid.setInputType(EditorInfo.TYPE_CLASS_TEXT);
                addname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                        switch (i){
                            case EditorInfo.IME_ACTION_NEXT :
                                addid.requestFocus();
                        }
                        return false;
                    }
                });
                addid.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                        switch (i) {
                            case EditorInfo.IME_ACTION_DONE :
                                final String id = addid.getText().toString();
                                final String name = addname.getText().toString();
                                new AsyncTaskCancelTimerTask(new AsyncTask<Void, Void, Void>() {
                                    @Override
                                    protected void onPreExecute() {
                                        dialog = ProgressDialog.show(mContext,"","등록중입니다");
                                        mContext.bindService(new Intent(mContext, MqttService.class), conn, Context.BIND_AUTO_CREATE);
                                        appdb.mqttinsert(id, addname.getText().toString());
                                    }

                                    @Override
                                    protected Void doInBackground(Void... voids) {
                                        while(ms == null){}
                                        ms.subscribeToTopic(id);
                                        ms.subscribeToTopic(id + "chat");
                                        while(!sucsub){}
                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(Void v) {
                                        dialog.dismiss();
                                        Toast.makeText(mContext,"등록에 성공하였습니다",Toast.LENGTH_SHORT).show();
                                        final View addsmsview = inflater.inflate(R.layout.layout_networkitem,null);
                                        TextView names = (TextView) addsmsview.findViewById(R.id.net_name);
                                        TextView topics = (TextView) addsmsview.findViewById(R.id.net_num);
                                        Button smsdelete = (Button) addsmsview.findViewById(R.id.net_delete);
                                        names.setText(name);
                                        topics.setText(id);
                                        smsdelete.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                appdb.mqttdelete(id);
                                                emerlist.removeView(addsmsview);
                                            }
                                        });
                                        emerlist.addView(addsmsview);
                                        mContext.unbindService(conn);
                                    }
                                }.execute(),10000,1000, new AsyncTaskCancelTimerTask.onCanceldo() {
                                    @Override
                                    public void onCancel() {
                                        dialog.dismiss();
                                        appdb.mqttdelete(id);
                                        Toast.makeText(mContext, "시간이 초과하여 등록에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                                    }
                                }).start();
                                userdialog.dismiss();
                        }
                        return false;
                    }
                });
                addbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String id = addid.getText().toString();
                        final String name = addname.getText().toString();
                        new AsyncTaskCancelTimerTask(new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected void onPreExecute() {
                                dialog = ProgressDialog.show(mContext,"","등록중입니다");
                                mContext.bindService(new Intent(mContext, MqttService.class), conn, Context.BIND_AUTO_CREATE);
                                appdb.mqttinsert(id, name);
                            }

                            @Override
                            protected Void doInBackground(Void... voids) {
                                while(ms == null){}
                                ms.subscribeToTopic(id);
                                ms.subscribeToTopic(id + "chat");
                                while(!sucsub){}
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void v) {
                                dialog.dismiss();
                                Toast.makeText(mContext,"등록에 성공하였습니다",Toast.LENGTH_SHORT).show();
                                final View addsmsview = inflater.inflate(R.layout.layout_networkitem,null);
                                TextView names = (TextView) addsmsview.findViewById(R.id.net_name);
                                TextView topics = (TextView) addsmsview.findViewById(R.id.net_num);
                                Button smsdelete = (Button) addsmsview.findViewById(R.id.net_delete);
                                names.setText(name);
                                topics.setText(id);
                                smsdelete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        appdb.mqttdelete(id);
                                        emerlist.removeView(addsmsview);
                                    }
                                });
                                emerlist.addView(addsmsview);
                                mContext.unbindService(conn);
                            }
                        }.execute(),10000,1000, new AsyncTaskCancelTimerTask.onCanceldo() {
                            @Override
                            public void onCancel() {
                                dialog.dismiss();
                                appdb.mqttdelete(id);
                                Toast.makeText(mContext, "시간이 초과하여 등록에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                            }
                        }).start();
                        userdialog.dismiss();
                    }
                });
            }
        });
        Cursor mqttlists = appdb.getmqtt();
        while (mqttlists.moveToNext()) {
            final String topic = mqttlists.getString(0);
            final String mqname = mqttlists.getString(1);
            final View addsmsview = inflater.inflate(R.layout.layout_networkitem,null);
            TextView name = (TextView) addsmsview.findViewById(R.id.net_name);
            TextView topics = (TextView) addsmsview.findViewById(R.id.net_num);
            Button smsdelete = (Button) addsmsview.findViewById(R.id.net_delete);
            name.setText(mqname);
            topics.setText(topic);
            smsdelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appdb.mqttdelete(topic);
                    emerlist.removeView(addsmsview);
                }
            });
            emerlist.addView(addsmsview);
        }
        return v;
    }

}
