package incheon.ourprotector.Tabs;

/**
 * Created by masi_ on 2016-11-30.
 */

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import incheon.ourprotector.R;
import incheon.ourprotector.System.Appdb;

@SuppressLint("ValidFragment")
public class Maps extends Fragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private Appdb appdb;
    private Context mContext;
    private View v;

    private Button mapmy,mapemer;
    private TextView maplog;
    private ProgressDialog dialog;

    public Maps(Context context) {
        this.mContext = context;
        appdb = new Appdb(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        try {
            v = inflater.inflate(R.layout.tab_map, null);
            mapmy = (Button) v.findViewById(R.id.map_my);
            mapemer = (Button) v.findViewById(R.id.map_emer);
            maplog = (TextView) v.findViewById(R.id.map_log);
            mapmy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MapSetting(false,null);
                }
            });
            mapemer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Cursor emerloca = appdb.getemerloca(appdb.lastemerlocanum());
                    while (emerloca.moveToNext()) {
                        int i = emerloca.getInt(0);
                        String name = emerloca.getString(1);
                        String data = emerloca.getString(2);
                        logset(name + " 의 최근 응급상황 위치");
                        if(emerloca.getPosition() == emerloca.getCount() - 1) {
                            try {
                                emerloca.close();
                                MapSetting(true, new JSONArray(data));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }catch (Exception e) {

        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onDestroyView () {
        super.onDestroyView();
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null) {
                parent.removeView(v);
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    public void logset(String s) {
        maplog.setText(s);
    }

    public void MapSetting(final boolean b, JSONArray array) {
        if(b){
            if(mMap != null) {
                mMap.clear();
            }
            for(int i = 0 ; i < array.length() ; i++){
                try {
                    JSONObject object = array.getJSONObject(i);
                    try {
                        double lati = object.getDouble("lati");
                        double loti = object.getDouble("loti");
                        String time = object.getString("time");
                        LatLng sydney = new LatLng(lati, loti);
                        mMap.addMarker(new MarkerOptions().position(sydney).title(time));
                        if(i == array.length() - 1){
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                            mMap.moveCamera(CameraUpdateFactory.zoomTo(15));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            if(mMap != null) {
                mMap.clear();
            }
            logset("나의 위치 기록");
            Cursor locations = appdb.getlocaions();
            while (locations.moveToNext()) {
                int i = locations.getInt(0);
                double lati = locations.getDouble(1);
                double loti = locations.getDouble(2);
                String time = locations.getString(3);
                LatLng sydney = new LatLng(lati, loti);
                mMap.addMarker(new MarkerOptions().position(sydney).title(time));
                if (locations.getPosition() == locations.getCount() - 1) {
                    Toast.makeText(mContext,time,Toast.LENGTH_SHORT).show();
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                    mMap.moveCamera(CameraUpdateFactory.zoomTo(15));
                    locations.close();
                }
            }
        }
    }
}
